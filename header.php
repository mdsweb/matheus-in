<!DOCTYPE html>
<html lang="pt">
<head>
    <!--SITE CONFIG-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#0052cc"> 

    <!--TAGS-->
	<meta charset="UTF-8">
    <title>Matheus Decleve</title>
    <meta name="description" content="testando description" />
    <meta name="title" content="" />
    <meta name="keywords" content="" />

    <!--META OG TAGS-->
    <meta property="og:title" content="Matheus Decleve" />
    <meta property="og:description" content="Desenvolvedor front-end" />
    <meta property="og:author" content="Matheus Decleve" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:url" content="http://www.matheus.in/" />
    <meta property="og:image" content="http://www.matheus.in/dist/images/screenshot.png" />

    <!--NOINDEX NOFOLLOW-->
    <meta name="robots" content="noindex, nofollow" />   

    <!--CRITICAL CSS-->    
    <style><?php echo file_get_contents( 'dist/style/critical.css' ); ?></style>      

    <!--MAIN CSS-->
    <link rel="stylesheet" href="dist/style/normalize.css">
    <link rel="stylesheet" href="dist/style/style.css">      
    
    <!--FAVICONS-->
    <link rel="apple-touch-icon" sizes="180x180" href="dist/images/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="dist/images/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="dist/images/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="shortcut icon" href="dist/images/favicons/favicon.png"> 
</head>
<body>

<div class="cursor-dot-outline"></div>
<div class="cursor-dot"></div>