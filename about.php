<?php include('header.php'); ?>
<?php include('menu.php'); ?>

<header>
	<div class="logo">		
		<span>MATHEUS</span>		
	</div>
	<div class="hamburguer">
		<div class="bar1"></div>
		<div class="bar2"></div>
		<span>MENU</span>
	</div>
	<div class="social">
		<a target="_blank" href="https://bitbucket.org/mdsweb/">bitbucket</a> /
		<a target="_blank" href="https://codepen.io/mdsweb/">codepen</a> /
		<a target="_blank" href="https://www.linkedin.com/in/matheus-decleve/">linkedin</a>
	</div>
	<div class="language">
		<a href="#!">PT</a> /
		<a href="#!">EN</a>
	</div>	
	<div class="container">	
		<span>About me</span>
		<h1>Matheus Decleve front-end developer, lives in Brazil</h1>
		<a href="mailto:me@matheus.in">me@matheus.in</a>			
	</div>
</header>

<main>

	<div class="about container">
		<div class="about__title">
			<h1>About</h1>
		</div>
		<div class="about__content">
			<p>Vanuit de Spoorzone, het creatieve hart van Tilburg, streven we onze passie na: bedrijven helpen online succesvol te zijn. We doen dit door maatwerk WordPress websites te maken waarin de bezoeker centraal staat. Wij zijn een internetbureau dat tot het uitsterste gaat en streeft naar perfectie. Zo overtreffen we telkens weer ieders verwachtingspatroon.</p>
			<a href="#!">more</a>
		</div>
    </div>
    
    <div class="about container">
		<div class="about__title">
			<h1>Education</h1>
		</div>
		<div class="about__content">
			<p>Vanuit de Spoorzone, het creatieve hart van Tilburg, streven we onze passie na: bedrijven helpen online succesvol te zijn. We doen dit door maatwerk WordPress websites te maken waarin de bezoeker centraal staat. Wij zijn een internetbureau dat tot het uitsterste gaat en streeft naar perfectie. Zo overtreffen we telkens weer ieders verwachtingspatroon.</p>
			<a href="#!">more</a>
		</div>
    </div>
    
    <div class="about container">
		<div class="about__title">
			<h1>Experience</h1>
		</div>
		<div class="about__content">
			<p>Vanuit de Spoorzone, het creatieve hart van Tilburg, streven we onze passie na: bedrijven helpen online succesvol te zijn. We doen dit door maatwerk WordPress websites te maken waarin de bezoeker centraal staat. Wij zijn een internetbureau dat tot het uitsterste gaat en streeft naar perfectie. Zo overtreffen we telkens weer ieders verwachtingspatroon.</p>
			<a href="#!">more</a>
		</div>
    </div>
    
    <div class="about container">
		<div class="about__title">
			<h1>Awwwards</h1>
		</div>
		<div class="about__content">
			<p>Vanuit de Spoorzone, het creatieve hart van Tilburg, streven we onze passie na: bedrijven helpen online succesvol te zijn. We doen dit door maatwerk WordPress websites te maken waarin de bezoeker centraal staat. Wij zijn een internetbureau dat tot het uitsterste gaat en streeft naar perfectie. Zo overtreffen we telkens weer ieders verwachtingspatroon.</p>
			<a href="#!">more</a>
		</div>
	</div>

</main>

<?php include ('footer.php'); ?>
