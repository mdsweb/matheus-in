<?php include('header.php'); ?>

<header class="404">
	<div class="logo">		
		<span>MATHEUS</span>		
	</div>
	<div class="hamburguer">
        <a href="index.php">Back to home →</a>
	</div>	
	<div class="language">
		<a href="#!">PT</a> /
		<a href="#!">EN</a>
	</div>
	
	<div class="container text-center">	
		<b>ERROR 404</b>
		<h1>Sorry, the page were you looking for doesn't exist.</h1>					
	</div>
</header>

<?php include ('footer.php'); ?>