<?php include('header.php'); ?>
<?php include('menu.php'); ?>

<header>
	<div class="logo">		
		<h1>PR<a></a>TON</h1>		
	</div>
	<div class="hamburguer">
		<div class="bar1"></div>
		<div class="bar2"></div>
		<span>MENU</span>
	</div>
	<div class="scroll">
		<a href="#scroll">
			<span>↓</span>
		</a>
	</div>
	<div class="container">	
		<span>Site de qualidade é Proton</span>
		<h1>Desenvolvimento de sites inovadores</h1>	
		<a href="#!">Cargas positivas</a>				
	</div>
</header>

<main>
	<div class="sobre container">
		<div class="sobre__title">
			<h2>Ipsum dolor sit amet</h2>
		</div>
		<div class="sobre__content">
			Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci accusamus assumenda vitae quod laudantium error, animi atque perferendis tempore reiciendis officia nulla expedita sequi, sint quasi. Dolores voluptatibus sit ratione.
		</div>
	</div>
	<div id="scroll" class="recent-works container">
		<div class="recent-works__item">
			<div class="recent-works__item__content">
				<b>Website</b>
				<span>Akme</span>
			</div>			
			 <div class="image">
				<img class="js-lazy-image" data-src="dist/images/akme.gif" />
				<noscript>
					<img data-src="dist/images/akme.gif" />
				</noscript>
			</div>
		</div>
		<div class="recent-works__item">
			<div class="recent-works__item__content">
				<b>Website</b>
				<span>Voavant</span>
			</div>			
			 <div class="image">
				<img class="js-lazy-image" data-src="dist/images/voavant.gif" />
				<noscript>
					<img data-src="dist/images/voavant.gif" />
				</noscript>
			</div>
		</div>
		<div class="recent-works__item">
			<div class="recent-works__item__content">
				<b>Website</b>
				<span>BAS Américas</span>
			</div>			
			 <div class="image">
				<img class="js-lazy-image" data-src="dist/images/bas-americas.gif" />
				<noscript>
					<img data-src="dist/images/bas-americas.gif" />
				</noscript>
			</div>
		</div>
	</div>

</main>

<?php include ('footer.php'); ?>
