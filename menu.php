<nav>
	<div class="content">			
		<div class="content__menu">
			<h1>Menu</h1>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="works.php">Works</a></li>					
				<li><a href="#!">Contact</a></li>
			</ul>
		</div>
		<div class="content__text">
			<h1>About</h1>	
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias tenetur placeat neque exercitationem voluptatibus asperiores laborum qui quo consectetur adipisci consequatur quos, minima iure, iusto aperiam voluptatem corporis voluptates nesciunt.</p>
			<a href="mailto:me@matheus.in">me@matheus.in</a>				
		</div>			
	</div>		
</nav>